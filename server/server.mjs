import { createServer } from 'http';
import * as fs from 'fs';
import { URL } from 'url';

import { WebSocketServer } from 'ws';
import { Server } from 'node-static';

import { Room } from './room.mjs';
import { makeId } from './id.mjs';

const PORT = 3000;

const testMovesFile = process.argv[2];
let testMoves = null;
if (testMovesFile) {
    testMoves = JSON.parse(fs.readFileSync(testMovesFile));
    console.log(`Using test move set from ${testMovesFile}`);
}

const statik = new Server('./public', {
    indexFile: "index.html"
});

const server = createServer(function(request, response) {
    request.addListener('end', () => {
        if (!request.url.includes(".")) {
            statik.serveFile("index.html", 200, {}, request, response);
        } else {
            statik.serve(request, response);
        }
    }).resume();
});

const wss = new WebSocketServer({ server: server, path: '/game', });
let rooms = new Map();

wss.on('connection', function connection(ws, req) {
    const url = new URL(req.url, `http://${req.headers.host}`);
    const clientId = url.searchParams.get("clientId");

    if (clientId === null || clientId === "") {
        ws.close();
        console.log('Client ID was not passed during websocket connection');
        return;
    }

    if (url.searchParams.has("id")) {
        const roomId = url.searchParams.get("id").toUpperCase();
        if (!rooms.has(roomId)) {
            console.log(`Room with ID '${roomId}' was not found`);
            return createRoom(ws, clientId);
        }

        const room = rooms.get(roomId);
        if (!room.canJoin(clientId)) {
            console.log("Room was not joinable:", roomId);
            return createRoom(ws, clientId);
        }

        room.join(ws, clientId);
        console.log(`${roomId}: Player joined with ID ${clientId}`);
    } else {
        createRoom(ws, clientId);
    }
});

function createRoom(ws, clientId) {
    const newId = makeId();
    const room = new Room(newId, ws, clientId, (id) => {
        rooms.delete(id);
        console.log(`${id}: Room closed`);
    });
    if (testMoves !== null) {
        room.useTestMoves(testMoves);
    }
    rooms.set(newId, room);
    console.log(`${newId}: Room created by ${clientId}`);
}

server.listen(PORT);
console.log(`Server running on port ${PORT}`);
