const charSet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'P', 'Q', 'R', 'S', 'T', 'W', 'X', 'Y', 'Z'];

export function makeId() {
    return sampleList(charSet) + sampleList(charSet) + sampleList(charSet) + sampleList(charSet);
}

function sampleList(list) {
    return list[Math.floor(Math.random()*list.length)];
}
