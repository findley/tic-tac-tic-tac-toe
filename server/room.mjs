import { Game, MARK_X, MARK_O } from './../public/game.mjs';

const CLOSE_TIMEOUT_SECONDS = 60 * 30;

export class Room {
    constructor(id, ws, clientId, onClose) {
        // Room state
        this.id = id;
        this.onClose = onClose;
        this.players = { x: null, o: null };
        this.connections = {}; // Map from clientId to ws connection
        this.rematchReady = { x: false, o: false };
        this.testMoves = null;
        this.closeTimer = null;

        // Game state
        this.game = null;
        this.started = false;
        this.paused = false;
        this.inPostGame = false;
        this.moveList = [];

        this.join(ws, clientId);
    }

    canJoin(clientId) {
        if (this.players.x === null || this.players.o === null) {
            return true;
        }

        if (this.players.x === clientId || this.players.o === clientId) {
            return true;
        }
    }

    join(ws, clientId) {
        const isReconnecting = clientId === this.players.x || clientId === this.players.o;
        if (this.players.x === null && !isReconnecting) {
            this.players.x = clientId;
        } else if (this.players.o === null && !isReconnecting) {
            this.players.o = clientId;
        }
        this.connections[clientId] = ws;

        if (this.closeTimer !== null) {
            clearTimeout(this.closeTimer);
            this.closeTimer = null;
        }

        this.send(ws, {
            type: "room_joined",
            id: this.id,
        });

        ws.on('message', (data) => this.handleMessage(data, clientId));
        ws.on('close', () => this.handleClose(clientId));

        if (isReconnecting) {
            if (this.started || this.inPostGame) {
                this.gameSync(ws, clientId);
            }
        }

        if (this.bothPlayersConnected()) {
            if (this.started || this.inPostGame) {
                this.resume();
            } else {
                this.start();
            }
        }
    }

    bothPlayersConnected() {
        return (
            this.players.x !== null &&
            this.players.o !== null &&
            this.connections.hasOwnProperty(this.players.x) &&
            this.connections.hasOwnProperty(this.players.o)
        );
    }

    handleClose(clientId) {
        if (clientId === this.players.x) {
            delete this.connections[clientId];
            this.rematchReady.x = false;
            console.log(`${this.id}: Player X disconnected`);
        } else if (clientId === this.players.o) {
            delete this.connections[clientId];
            this.rematchReady.o = false;
            console.log(`${this.id}: Player O disconnected`);
        }

        if (Object.keys(this.connections).length === 0) {
            this.closeTimer = setTimeout(() => {
                this.close();
            }, 1000 * CLOSE_TIMEOUT_SECONDS);
            console.log(`${this.id}: All connections lost, room will close in ${CLOSE_TIMEOUT_SECONDS} seconds`);
        } else {
            this.broadcast({
                type: "game_pause"
            });
            this.paused = true;
            console.log(`${this.id}: Game paused`);
        }
    }

    handleMessage(data, clientId) {
        const mark = this.getMarkForClient(clientId);
        if (mark === "") {
            console.error("Failed to lookup mark based on socket");
            return;
        }

        try {
            const msg = JSON.parse(data);
            switch (msg.type) {
                case "play_square":
                    this.handlePlaySquare(mark, msg.i, msg.j);
                    break;
                case "rematch":
                    this.handleRematch(mark);
                    break;
            }
            console.log(`${this.id}: message from ${mark}:`, msg);
        } catch (e) {
            console.log(`${this.id}: Could not parse message from client:`, e);
        }
    }

    handleRematch(mark) {
        if (!this.inPostGame) {
            return;
        }

        this.rematchReady[mark] = true;

        let count = 0;
        if (this.rematchReady.x) {
            count += 1;
        }
        if (this.rematchReady.o) {
            count += 1;
        }

        if (count < 2) {
            this.broadcast({
                type: "rematch_ready_count",
                count: count,
            });
        } else {
            // Swap x and o
            [this.players.x, this.players.o] = [this.players.o, this.players.x];

            this.start();
        }
    }

    handlePlaySquare(player, i, j) {
        const clientId = this.players[player];
        if (!this.started) {
            this.send(this.connections[clientId], {
                type: "invalid_move",
                msg: "Game is not ready",
            });

            return;
        }
        if (this.game.turn !== player) {
            this.send(this.connections[clientId], {
                type: "invalid_move",
                msg: "It's not your turn",
            });

            return;
        }

        if (this.game.playSquare(i, j)) {
            this.broadcast({
                type: "play_square",
                i: i,
                j: j,
            });
            this.moveList.push([i, j]);
        } else {
            this.send(this.connections[clientId], {
                type: "invalid_move",
                msg: "That move is not allowed",
            });

            return;
        }

        if (this.game.isOver()) {
            this.started = false;
            this.inPostGame = true;
        }
    }

    close() {
        for (const id in this.connections) {
            this.connections[id].close();
        }

        this.onClose(this.id);
    }

    start() {
        this.game = new Game();
        this.moveList = [];
        this.paused = false;
        this.started = true;
        this.inPostGame = false;
        this.rematchReady = { x: false, o: false };

        this.send(this.connections[this.players.x], {
            type: "game_start",
            mark: "x"
        });
        this.send(this.connections[this.players.o], {
            type: "game_start",
            mark: "o"
        });
        console.log(`${this.id}: Game started`);

        if (this.testMoves !== null) {
            this.playTest();
        }
    }

    resume() {
        this.paused = false;
        this.send(this.connections[this.players.x], {
            type: "game_resume",
            mark: "x"
        });
        this.send(this.connections[this.players.o], {
            type: "game_resume",
            mark: "o"
        });
        console.log(`${this.id}: Game unpaused`);
    }

    gameSync(ws, clientId) {
        this.send(ws, {
            type: "game_sync",
            moves: this.moveList,
            started: this.started,
            paused: this.paused,
            mark: this.getMarkForClient(clientId),
        });
    }

    broadcast(msg) {
        for (const id in this.connections) {
            this.connections[id].send(JSON.stringify(msg));
        }
    }

    send(ws, msg) {
        ws.send(JSON.stringify(msg));
    }

    getMarkForClient(clientId) {
        if (clientId === this.players.x) {
            return MARK_X;
        }
        if (clientId === this.players.o) {
            return MARK_O;
        }

        return "";
    }

    useTestMoves(moves) {
        this.testMoves = moves;
    }

    async playTest() {
        for (const [index, [i, j]] of this.testMoves.entries()) {
            await new Promise(r => setTimeout(r, 100));
            const mark = index % 2 === 0 ? MARK_X : MARK_O;
            this.handlePlaySquare(mark, i, j);
        }
    }
}
