# Tic-Tac-Tic-Tac-Toe
An online web-based ultimate tic-tac-toe game.

A goal of this project is to keep things as simple as possible. Nice things like
Typescript, React, and Express were intentionally given up to avoid having an
build process and many dependencies. The nodejs server shares game logic with
the front-end. The server uses minimal npm dependencies (ws has 1 dependency and
node-static has 3 dependencies).

<img src="screenshot.png" alt="screenshot" width="500" />

## Local Setup
### Clone the repository
```bash
$ git clone git@gitlab.com:findley/tic-tac-tic-tac-toe.git
$ cd tic-tac-tic-tac-toe
```

### Install npm dependencies
```bash
$ npm install
```

### Run the server
```
$ npm start
```

### Testing
The server will accept an optional "move list" json file as an argument. When
provided any time a room starts the server will auto-play the move list. This is
helpful for partially automated testing, especially in a draw scenario which can
be time-consuming to produce manually.

```bash
$ npm start test/draw.json
```

The move list file should contain an array of moves, where each move is an array
with two elements `[i, j]`. The first number selects the outer board square, and
the second number selects the sub-board square. Squares are numbered 0-8 left to
right top to bottom.

## License
[MIT](LICENSE)
