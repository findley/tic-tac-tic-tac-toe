FROM node:16-alpine

WORKDIR /opt/tictactictactoe

COPY package*.json ./
RUN npm ci

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
