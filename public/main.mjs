import { MARK_X, MARK_O } from './game.mjs';
import { Room } from './room.mjs';

init();

function init() {
    let clientId = window.localStorage.getItem('clientId');
    if (clientId === null) {
        clientId = uuidv4();
        window.localStorage.setItem('clientId', clientId);
    }
    const room = new Room(clientId);

    room.onJoin((id) => {
        if (getGameIdFromUrl() !== "") {
            history.replaceState({ id }, "Tic Tac Tic Tac Toe", `/g/${id}`);
        } else {
            history.pushState({ id }, "Tic Tac Tic Tac Toe", `/g/${id}`);
        }
    });
    room.onClose(() => renderStatus(room));
    room.onDraw(() => renderRoom(room));

    attachClickHandlers(room);

    window.addEventListener('popstate', (event) => {
        setupRoom(room, event?.state?.id ?? "");
        renderRoom(room);
    });

    const gameId = getGameIdFromUrl();
    setupRoom(room, gameId);
}

function setupRoom(room, gameId) {
    room.close();
    if (gameId === "local") {
        room.startLocal();
    } else if (gameId !== "") {
        room.connect(getSocketUrl(gameId));
    }
}

function attachClickHandlers(room) {
    const squares = document.querySelectorAll('.inner-board>.box');
    for (const square of squares) {
        square.addEventListener('click', (event) => {
            const i = parseInt(event.target.parentElement.closest("[data-pos]").getAttribute("data-pos"));
            const j = parseInt(event.target.getAttribute("data-pos"));
            room.playSquare(i, j);
        });
    }

    document.getElementById("right-header").addEventListener('click', () => room.playAgain());
    document.getElementById("create-room").addEventListener('click', () => {
        room.connect(getSocketUrl(""));
    });
    document.getElementById("join-room").addEventListener('click', (event) => {
        event.preventDefault();
        const joinIdEl = document.getElementById("join-code");
        const joinId = joinIdEl.value.toUpperCase();
        if (joinId.length !== 4) {
            joinIdEl.focus();
            joinIdEl.classList.add("input-error");
            return;
        }

        joinIdEl.classList.remove("input-error");
        room.connect(getSocketUrl(joinId));
    });
    document.getElementById("local-game").addEventListener('click', () => {
        history.pushState({ id: 'local' }, "Tic Tac Tic Tac Toe", `/g/local`)
        room.startLocal();
    });
}

function getGameIdFromUrl() {
    const pathParts = window.location.pathname.split('/');
    let id = "";
    if (pathParts.length === 3 && pathParts[1] === "g") {
        id = pathParts[2]
    }

    return id;
}

function getSocketUrl(id) {
    let protocol = "ws:";
    if (location.protocol === "https:") {
        protocol = "wss:";
    }
    return `${protocol}//${location.host}/game?id=${id}`;
}

function renderRoom(room) {
    if (room.connected || room.started) {
        document.querySelector(".menu").classList.add('hide');
        document.querySelector(".outer-board").classList.remove('hide');
        document.querySelector(".header").classList.remove('hide');
        renderGame(room);
    } else {
        document.querySelector(".menu").classList.remove('hide');
        document.querySelector(".outer-board").classList.add('hide');
        document.querySelector(".header").classList.add('hide');
    }
}

function renderGame(room) {
    renderStatus(room);
    const gameEl = document.querySelector(".game")

    gameEl.setAttribute("data-turn", room.getTurn());
    if (room.isMyTurn()) {
        gameEl.setAttribute("data-active", "true");
    } else {
        gameEl.removeAttribute("data-active");
    }
    if (room.isGameOver()) {
        gameEl.classList.add("bg-win");
    } else {
        gameEl.classList.remove("bg-win");
    }

    const innerBoards = room.game?.innerBoards ?? [];
    for (const [i, board] of innerBoards.entries()) {
        const outerBoxEl = document.querySelector(`.outer-board>.box[data-pos="${i}"]`);
        const currentBoard = room.currentBoard();

        if (!room.isGameOver() && board.winner === "" && (currentBoard === i || currentBoard === -1)) {
            outerBoxEl.classList.add("bg-pulse");
        } else {
            outerBoxEl.classList.remove("bg-pulse");
        }

        renderBoard(i, outerBoxEl, board, room.game);
    }
}

function renderBoard(i, outerBoxEl, board, game) {
    outerBoxEl.querySelector("div.winner").className = `winner ${board.winner}`;
    if (board.winner !== "") {
        outerBoxEl.querySelector(".inner-board").classList.add("board-completed");
    } else {
        outerBoxEl.querySelector(".inner-board").classList.remove("board-completed");
    }

    for (let j = 0; j < 9; j++) {
        let newClasses = "box";

        if (board.grid[j] === MARK_X) {
            newClasses += " x";
        } else if (board.grid[j] === MARK_O) {
            newClasses += " o";
        }

        if (game.isLastMove(i, j)) {
            newClasses += " last-move";
        }

        outerBoxEl.querySelector(`div.box [data-pos="${j}"]`).className = newClasses;
    }
}

function renderStatus(room) {
    const [status, classes, title] = getStatusString(room);
    document.title = title;
    const statusEl = document.getElementById("game-status");
    statusEl.innerText = status;
    statusEl.className = classes;

    const rightHeader = document.getElementById("right-header");
    rightHeader.style.cursor = "initial";
    if (!room.started) {
        rightHeader.innerText = window.location.host + window.location.pathname + window.location.search;
    } else if (room.isGameOver() && !room.paused) {
        if (!room.rematchReady) {
            if (room.local) {
                rightHeader.style.cursor = "pointer";
                rightHeader.innerText = `↻ Play Again`;
            } else {
                rightHeader.style.cursor = "pointer";
                rightHeader.innerText = `↻ Play Again (${room.rematchReadyCount}/2)`;
            }
        } else {
            rightHeader.innerText = `Ready for Rematch (${room.rematchReadyCount}/2)`;
        }
    } else {
        rightHeader.innerText = "";
    }
}

function getStatusString(room) {
    const defaultTitle = "Tic Tac Tic Tac Toe";

    if (!room.connected && !room.local) {
        return ["Disconnected - Please Refresh", "", defaultTitle];
    }
    if (!room.started) {
        return ["Waiting for Opponent", "", defaultTitle];
    }
    if (room.isGameOver() && room.local) {
        return [`${room.game.winner().toUpperCase()} Wins!`, "", defaultTitle];
    }
    if (room.isWinner()) {
        return ["Victory!", "", defaultTitle];
    }
    if (room.isLoser()) {
        return ["Defeat", "", defaultTitle];
    }
    if (room.isDraw()) {
        return ["Draw!", "", defaultTitle]
    }
    if (room.isMyTurn()) {
        if (room.local) {
            return [`${room.getTurn().toUpperCase()} Turn`, "", defaultTitle];
        }

        return ["Your Turn", "grow-text",  "🟢 Your Turn! - Tic Tac Tic Tac Toe"];
    }
    if (room.paused) {
        return ["Paused - Waiting for Opponent to Reconnect", "", defaultTitle];
    }

    return ["Opponent's Turn", "", defaultTitle];
}

function uuidv4() {
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  );
}
