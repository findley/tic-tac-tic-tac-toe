export const MARK_X = "x";
export const MARK_O = "o";
export const MARK_DRAW = "d";

// 0  1  2
// 3  4  5
// 6  7  8
const winningLines = [
    [0, 1, 2],
    [3, 4, 5],
    [6, 7, 8],

    [0, 3, 6],
    [1, 4, 7],
    [2, 5, 8],

    [0, 4, 8],
    [2, 4, 6],
];

class Board {
    constructor() {
        this.grid = new Array(9);
        this.grid.fill("", 0);
        this.winner = "";
    }

    playSquare(i, mark) {
        if (!this.isMoveValid(i, mark)) {
            return false;
        }

        this.makeMark(i, mark);
        this.updateState();

        return true;
    }

    isMoveValid(i, mark) {
        if (mark === MARK_DRAW) {
            return true;
        }

        if (this.winner !== "") {
            return false;
        }

        if (i < 0 || i > 8) {
            return false;
        }

        if (this.grid[i] !== "") {
            return false;
        }

        return true;
    }

    makeMark(i, mark) {
        this.grid[i] = mark;
    }

    updateState() {
        for (const [a, b, c] of winningLines) {
            if (this.grid[a] !== "" && this.grid[a] !== MARK_DRAW && this.grid[a] === this.grid[b] && this.grid[b] === this.grid[c]) {
                this.winner = this.grid[a];
                return;
            }
        }

        for (const m of this.grid) {
            if (m === "") {
                return;
            }
        }

        this.winner = MARK_DRAW;
    }
}

export class Game {
    constructor() {
        this.lastMove = [-1, -1];
        this.turn = MARK_X;
        this.currentBoard = -1;
        this.outerBoard = new Board();
        this.innerBoards = new Array(9);
        for (let i = 0; i < 9; i++) {
            this.innerBoards[i] = new Board();
        }
    }

    playSquare(i, j) {
        if (!this.isMoveValid(i)) {
            return false;
        }

        if (!this.innerBoards[i].playSquare(j, this.turn)) {
            return false;
        }

        if (this.innerBoards[i].winner === this.turn) {
            this.outerBoard.playSquare(i, this.turn);
        } else if (this.innerBoards[i].winner === MARK_DRAW) {
            this.outerBoard.playSquare(i, MARK_DRAW);
        }

        this.currentBoard = j;
        if (this.innerBoards[j].winner !== "") {
            this.currentBoard = -1;
        }

        this.lastMove = [i, j];
        this.turn = this.turn === MARK_X ? MARK_O : MARK_X;

        return true;
    }

    isMoveValid(i) {
        if (this.currentBoard >= 0 && i !== this.currentBoard) {
            return false;
        }

        if (this.outerBoard.winner !== "") {
            return false;
        }

        if (i < 0 || i > 8) {
            return false;
        }

        return true;
    }

    isOver() {
        return this.outerBoard.winner !== "";
    }

    winner() {
        return this.outerBoard.winner;
    }

    isLastMove(i, j) {
        return i == this.lastMove[0] && j === this.lastMove[1];
    }
}
