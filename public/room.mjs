import { Game, MARK_DRAW, MARK_X, MARK_O } from './game.mjs';

export class Room {
    constructor(clientId) {
        this.clientId = clientId;

        this.connecting = false;
        this.connected = false;
        this.reconnectTimer = null;

        this.socket = null;
        this.id = null;
        this.local = false;

        this.joinedCallbacks = [];
        this.closeCallbacks = [];
        this.drawCallbacks = [];
        this.reset("");

        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleMessage = this.handleMessage.bind(this);
    }

    close() {
        this.reset();
        if (this.connected && this.socket !== null) {
            this.socket.removeEventListener("open", this.handleOpen);
            this.socket.removeEventListener("close", this.handleClose);
            this.socket.removeEventListener("message", this.handleMessage);
            this.connecting = false;
            this.connected = false;
            this.socket.close();
        }
        this.socket = null;
        this.local = false;
    }

    reset() {
        this.started = false;
        this.paused = false;
        this.mark = "";
        this.rematchReady = false;
        this.rematchReadyCount = 0;
        this.game = new Game();
        this.url = "";
    }

    start(mark) {
        this.reset();
        this.started = true;
        this.mark = mark;
    }

    onDraw(fn) {
        this.drawCallbacks.push(fn);
    }

    onClose(fn) {
        this.closeCallbacks.push(fn);
    }

    onJoin(fn) {
        this.joinedCallbacks.push(fn);
    }

    playSquare(i, j) {
        if (!this.canPlaySquare(i)) {
            return;
        }

        if (this.local) {
            this.game.playSquare(i, j);
            this.triggerDraw();
        } else {
            this.socket.send(JSON.stringify({
                type: 'play_square',
                i: i,
                j: j,
            }));
        }
    }

    canPlaySquare(i) {
        if (!this.started || (!this.connected && !this.local)) {
            return false;
        }

        if (this.mark !== this.game.turn && !this.local) {
            return false;
        }

        if (this.game.currentBoard >= 0 && this.game.currentBoard != i) {
            return false;
        }

        return true;
    }

    playAgain() {
        if (!this.game.isOver() || this.paused) {
            return;
        }

        if (this.local) {
            this.startLocal();
        } else {
            if (this.rematchReady) {
                return false;
            }

            this.rematchReady = true;
            this.socket.send(JSON.stringify({type: 'rematch'}));
        }
    }

    startLocal() {
        this.start("");
        this.started = true;
        this.local = true;
        this.paused = false;

        this.triggerDraw();
    }

    connect(url) {
        if (this.local) {
            return;
        }

        this.connecting = true;
        this.url = url + `&clientId=${this.clientId}`;
        this.socket = new WebSocket(this.url);
        this.socket.addEventListener("open", this.handleOpen);
        this.socket.addEventListener("close", this.handleClose);
        this.socket.addEventListener("message", this.handleMessage);
    }

    handleOpen() {
        clearInterval(this.reconnectTimer);
        this.reconnectTimer = setInterval(() => {
            if (!this.connected && !this.connecting) {
                this.connect(this.url);
            }
        }, 500);
        this.connected = true;
        this.connecting = false;
    }

    handleClose() {
        this.started = false;
        this.connected = false;
        this.connecting = false;
        this.paused = false;
        this.closeCallbacks.forEach(fn => fn());
    }

    handleMessage(event) {
        const msg = JSON.parse(event.data);

        switch (msg.type) {
            case "room_joined":
                this.id = msg.id;
                this.joinedCallbacks.forEach(fn => fn(msg.id));
                break;
            case "game_start":
                this.start(msg.mark);
                break;
            case "game_pause":
                this.paused = true;
                break;
            case "game_resume":
                this.paused = false;
                this.started = true;
                this.mark = msg.mark;
                break;
            case "game_sync":
                this.handleGameSync(msg);
                break;
            case "play_square":
                this.game.playSquare(msg.i, msg.j);
                break;
            case "rematch_ready_count":
                this.rematchReadyCount = msg.count;
        }

        this.triggerDraw();
    }

    handleGameSync(msg) {
        this.mark = msg.mark;
        for (const [i, j] of msg.moves) {
            this.game.playSquare(i, j);
        }
        this.started = msg.started;
        this.paused = msg.paused;
    }

    isWinner() {
        if (this.local) {
            return false;
        }

        return this.game.winner() === this.mark;
    }

    isLoser() {
        if (this.local) {
            return false;
        }

        const winner = this.game.winner();
        return this.mark === MARK_X ? winner === MARK_O : winner === MARK_X;
    }

    isDraw() {
        return this.game.winner() === MARK_DRAW;
    }

    isMyTurn() {
        return this.getTurn() === this.mark || this.local;
    }

    getTurn() {
        return this.game.turn;
    }

    isGameOver() {
        return this.game.isOver();
    }

    currentBoard() {
        return this.game.currentBoard;
    }

    triggerDraw() {
        this.drawCallbacks.forEach(fn => fn());
    }
}
